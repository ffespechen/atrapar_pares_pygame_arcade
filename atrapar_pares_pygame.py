#Icons made https://www.flaticon.com/authors/smashicons
#Fondos descargados de 
# http://wallpaperswide.com/drifting-wallpapers.html
# http://wallpaperswide.com/success-wallpapers.html
# http://wallpaperswide.com/love_game_over-wallpapers.html

import pygame 
from pygame.locals import *
import random


ancho = 640
alto = 480
velocidad = 5
fondo = pygame.image.load('fondo_espacio.jpg')
fondo_game_over = pygame.image.load('game_over.jpg')
fondo_ganador = pygame.image.load('ganador.jpg')

cantidad_objetivos = 20
reloj = pygame.time.Clock()

random.seed()


#-- Clases

class Personaje():
    """Personaje que captura los números pares"""    
    def __init__(self, x, y, path_imagen):
        self.x = x
        self.y = y
        self.ancho_p = 32
        self.alto_p = 32
        self.imagen = pygame.image.load(path_imagen)
    
    def dibujar(self, ventana):
        ventana.blit(self.imagen, (self.x, self.y))


class Objetivo():
    """Objetivos de captura"""
    def __init__(self):
        self.radio = 16
        
        self.center_x = random.randint(0+2*self.radio, ancho-2*self.radio)
        self.center_y = random.randint(0+2*self.radio, alto+2*self.radio)
        
        self.x = self.center_x - self.radio
        self.y = self.center_y - self.radio
        
        self.valor = random.choice(range(1,100))
        letra = pygame.font.SysFont(None, 24)
        self.imagen = letra.render(str(self.valor), True, (255,0,0))
        
        if( self.valor%2==0 ):
            self.is_par = True
            self.color = (0,255,0)
        else:
            self.is_par = False
            self.color = (255,255,0)
        
    def dibujar(self, ventana):
        desp_x = random.randint(-12,12)
        desp_y = random.randint(-12,12)
        
        self.center_x += desp_x
        self.center_y += desp_y
        
        if (self.center_x < 0):
            self.center_x = 0
        if (self.center_x > ancho):
            self.center_x = ancho
            
        if (self.center_y < 0):
            self.center_y = 0
        if (self.center_y > alto):
            self.center_y = alto
        
        self.x = self.center_x - self.radio
        self.y = self.center_y - self.radio
        
        pygame.draw.circle(ventana, self.color, (self.center_x, self.center_y ), self.radio)
        ventana.blit(self.imagen, (self.x+self.radio//2, self.y+self.radio//2))
        
        
        
def main():

    #-- Inicializando la pantalla
    pantalla = pygame.display.set_mode((ancho, alto))
    pygame.display.set_caption('Pygame Capturar los PARES')
    
    #-- Inicializo las letras para mensajes
    letra_mensaje = pygame.font.Font(None, 30)

    
    lista_objetivos = []
    cantidad_pares = 0
    vidas = 3
    
    protagonista = Personaje(ancho//2, alto//2, 'ufo.png')
    
    #-- objetivos
    
    for i in range(cantidad_objetivos):
        objetivo = Objetivo()
        if objetivo.is_par:
            print(objetivo.valor, objetivo.x, objetivo.y)
            cantidad_pares += 1
        lista_objetivos.append(objetivo)
    
    
    print(cantidad_pares)
    
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        
        reloj.tick(27)
        
        teclas = pygame.key.get_pressed()
        
        if(teclas[pygame.K_LEFT]):
            # print('Tecla Izquierda')
            if(protagonista.x - velocidad < 0):
                protagonista.x = 0
            else:
                protagonista.x -= velocidad
        
        if(teclas[pygame.K_RIGHT]):
            # print('Tecla Derecha')
            if(protagonista.x + protagonista.ancho_p + velocidad > ancho):
                protagonista.x = ancho - protagonista.ancho_p
            else:
                protagonista.x += velocidad
        
        if(teclas[pygame.K_UP]):
            # print('Tecla Arriba')
            if(protagonista.y - velocidad < 0):
                protagonista.y = 0
            else:
                protagonista.y -= velocidad
        
        if(teclas[pygame.K_DOWN]):
            # print('Tecla Abajo')
            if(protagonista.y + protagonista.alto_p + velocidad > alto):
                protagonista.y = alto - protagonista.alto_p
            else:
                protagonista.y += velocidad
        
        
        #-- Acualizar el fondo de la ventana
        pantalla.blit(fondo, (0,0))
        
        #-- Dibujar personajes
        protagonista.dibujar(pantalla)
        
        
        #-- Dibujar objetivos
        for objetivo in lista_objetivos:
            
            if(protagonista.x+protagonista.ancho_p//2 >= objetivo.x and 
               protagonista.x+protagonista.ancho_p//2 <= objetivo.x + objetivo.radio*2 and 
               protagonista.y+protagonista.alto_p//2 >= objetivo.y and 
               protagonista.y+protagonista.alto_p//2 <= objetivo.y + objetivo.radio*2):
                
                if(objetivo.is_par):
                    print('PUNTO A FAVOR')
                    cantidad_pares -= 1
                    
                else:
                    vidas -= 1
                
                lista_objetivos.pop(lista_objetivos.index(objetivo))
            
            else:
                objetivo.dibujar(pantalla)                
        
        
        img_letra = letra_mensaje.render('Vidas: '+str(vidas), True, (255,255,255), (0,0,0))
        pantalla.blit(img_letra, (10,10))
        
        
        if(vidas <= 0):
            pantalla.blit(fondo_game_over, (0,0))
            pygame.display.update()
            pygame.time.wait(3000)
            pygame.quit()
            return 0
        
        if cantidad_pares == 0:
            pantalla.blit(fondo_ganador, (0,0))
            pygame.display.update()
            pygame.time.wait(3000)
            pygame.quit()
            return 0
        
        pygame.display.update()
        
     
    pygame.quit()
    return 0


if __name__ == '__main__':
    pygame.init()
    main()
